Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: VAVOOM
Upstream-Contact: Janis Legzdinsh <vavoom@vavoom-engine.com>
Source: http://vavoom-engine.com/

Files: progs/common/linespec/ActorMover.vc progs/common/linespec/AimingCamera.vc progs/common/linespec/InterpolationPoint.vc progs/common/linespec/InterpolationSpecial.vc progs/common/linespec/LookAtCamera.vc progs/common/linespec/ParticleFountain.vc progs/common/linespec/PathFollower.vc progs/common/linespec/SecurityCamera.vc progs/common/linespec/SoundSequence.vc
Copyright: 1998-2006 Randy Heit
           1999-2006 Janis Legzdinsh
License: BSD-3-clause and GPL-2+

License: BSD-3-clause
 All rights reserved
 .
 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following
 conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived
 from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,
 BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

Files: *
Copyright: 1990-2012 Janis Legzdinsh
License: GPL-2+

Files: source/fs_zip.cpp
Copyright: 1999-2006 Janis Legzdinsh
License: GPL-2+ or Zlib

Files: utils/vlumpy/*
Copyright: 1999-2006 Janis Legzdinsh
License: Zlib

License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
 claim that you wrote the original software. If you use this software
 in a product, an acknowledgment in the product documentation would be
 appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not be
 misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source distribution.

Files: debian/*
Copyright: 2010-2012 gustavo panizzo <gfa@zumbi.com.ar>
License: GPL-2+

License: GPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 On Debian systems, the complete text of the GNU General Public License
 can be found in `/usr/share/common-licenses/GPL-2'.
